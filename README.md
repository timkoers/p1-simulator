## USAGE

This software is used to simulate the P1 connection to a energy reader such as Toon.

For more information, please check out [this](https://www.domoticaforum.eu/viewtopic.php?f=103&t=12109&start=45) thread.

## INSTALLATION

Make sure that Python 2.7 is installed on your system

Python needs the following pacakges
pip install pyserial crcmod  paho-mqtt parse

If pip is not installed properly, run
```
python -m ensurepip --default-pip
```
Then install the packages with
```
python -m pip install pyserial crcmod paho-mqtt parse
```

## MQTT INPUT 

```
1-0:0.0.0*255(xxxxxxxx)
1-0:0.2.0*255(1.03)
0-0:C.1.0*255(xxxxxxxx)
0-0:C.1.6*255(FDF5)
0-0:C.51.1*255(0)
0-0:C.51.3*255(0)
0-0:C.51.5*255(0)
1-0:31.7.0*255(0.55*A)
1-0:51.7.0*255(1.61*A)
1-0:71.7.0*255(2.46*A)
1-0:31.6.0*255(31.58*A)
1-0:51.6.0*255(39.13*A)
1-0:71.6.0*255(28.48*A)
1-0:32.7.0*255(229.3*V)
1-0:52.7.0*255(233.5*V)
1-0:72.7.0*255(232.1*V)
1-0:33.7.0*255(0.796)
1-0:53.7.0*255(0.703)
1-0:73.7.0*255(0.938)
1-0:14.7.0*255(50.01*Hz)
1-0:1.8.0*255(0004984.248*kWh)
1-0:1.8.1*255(0002809.866*kWh)
1-0:1.8.2*255(0002174.382*kWh)
1-0:2.8.0*255(0000793.849*kWh)
1-0:2.8.1*255(0000189.205*kWh)
1-0:2.8.2*255(0000604.643*kWh)
0-0:F.F.0*255(0000000)
1-0:C.53.1*255(0000000.000*kWh)
1-0:C.53.2*255(0000000.000*kWh)
1-0:C.53.3*255(0005778.098*kWh)

```

## COPYRIGHT
You are allowed to use this piece of software for non-commercial purposes. 
You are not allowed to use this piece of software for commercial or illegal purposes.

## LICENSE
Non-Profit Open Software License version 3.0 (NPOSL-3.0)
[https://opensource.org/licenses/NPOSL-3.0](https://opensource.org/licenses/NPOSL-3.0)
