#!/usr/bin/env python
# Python script to retrieve and parse a DSMR telegram from a P1 port

import re
import sys
import serial
import crcmod.predefined
import time
import datetime
import ssl
import paho.mqtt.client as mqtt
import requests
from secrets import solaredge
from parse import parse

# Program variables
# The true telegram ends with an exclamation mark after a CR/LF
pattern = re.compile(b'\r\n(?=!)')
#pattern = re.compile(b'\n(?=!)')
# According to the DSMR spec, we need to check a CRC16
crc16 = crcmod.predefined.mkPredefinedCrcFun('crc16')
# Create an empty telegram
checksum_found = False
good_checksum = False
checksum = 0
#Serial port configuration
ser = serial.Serial()
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS
ser.parity = serial.PARITY_NONE
ser.stopbits = serial.STOPBITS_ONE
ser.xonxoff = 0
ser.rtscts = 0
ser.timeout = 1
ser.write_timeout = 1.0
ser.port = "/dev/ttyUSB0"
ser.open()
# Only do it with the test message now
#0-0:1.0.0(180802163616W)\r\n\

previousMessage = 0

previousActiveEnergyImport = 0# 1.8.0
previousActiveEnergyExport = 0 # 2.8.0

activeEnergyImport = 0 # 1.8.0
activeEnergyImportLowTarrif = 0 # 1.8.1
activeEnergyImportNormalorHighTarrif = 0 # 1.8.2

previousActiveEnergyImportLowTarrif = 0 # 1.8.1
previousActiveEnergyImportNormalorHighTarrif = 0 # 1.8.2

activeEnergyExport = 0 # 2.8.0
previousActiveEnergyExportLowTarrif = 0 # 2.8.1
previousActiveEnergyExportNormalorHighTarrif = 0 # 2.8.2

activeEnergyExportLowTarrif = 0 # 2.8.1
activeEnergyExportNormalorHighTarrif = 0 # 2.8.2

voltagePhaseOne = 0 # 32.7.0
voltagePhaseTwo = 0 # 52.7.0
voltagePhaseThree = 0 # 72.7.0

currentPhaseOne = 0 # 31.7.0
currentPhaseTwo = 0 # 51.7.0
currentPhaseThree = 0 # 71.7.0

activeImportPowerPhaseOne = 0 # 21.7.0
activeImportPowerPhaseTwo = 0 # 41.7.0
activeImportPowerPhaseThree = 0 # 61.7.0

activeExportPowerPhaseOne = 0 # 22.7.0
activeExportPowerPhaseTwo = 0 # 42.7.0
activeExportPowerPhaseThree = 0 # 62.7.0

actualPowerDeliveredToClient = 0 # 1.7.0 (kW)
actualPowerReceivedFromClient = 0 # 2.7.0 (kW)

instantaneousActivePowerImportL1 = 0 # 21.7.0 (kW) (+P)
instantaneousActivePowerImportL2 = 0 # 41.7.0 (kW) (+P)
instantaneousActivePowerImportL3 = 0 # 61.7.0 (kW) (+P)

instantaneousActivePowerExportL1 = 0 # 22.7.0 (kW) (-P)
instantaneousActivePowerExportL2 = 0 # 42.7.0 (kW) (-P)
instantaneousActivePowerExportL3 = 0 # 62.7.0 (kW) (-P)

activeTarrif = 1 # 0.96.14

importWattHour = 0
exportWattHour = 0

solarPanelGeneration = 0
gasReading = 0

powerImportedHistory = [] # 1.7.0
powerExportedHistory = [] # 2.7.0

debug = False
def parseTelegram(mqttTelegram):
    if debug:
        print("Parsing data")
    
    global activeEnergyImport
    global activeEnergyImportLowTarrif
    global activeEnergyImportNormalorHighTarrif
    global previousActiveEnergyImportLowTarrif
    global previousActiveEnergyImportNormalorHighTarrif

    global activeEnergyExport
    global activeEnergyExportLowTarrif
    global activeEnergyExportNormalorHighTarrif
    global previousActiveEnergyExportLowTarrif
    global previousActiveEnergyExportNormalorHighTarrif
    
    global previousActiveEnergyImport
    global previousActiveEnergyExport

    global voltagePhaseOne
    global voltagePhaseTwo
    global voltagePhaseThree
    
    global currentPhaseOne
    global currentPhaseTwo
    global currentPhaseThree
    
    global actualPowerDeliveredToClient
    global actualPowerReceivedFromClient
    
    global pattern
    global previousMessage
    global activeTarrif

    global importWattHour
    global exportWattHour
        
    global instantaneousActivePowerImportL1
    global instantaneousActivePowerImportL2
    global instantaneousActivePowerImportL3
    
    global instantaneousActivePowerExportL1
    global instantaneousActivePowerExportL2
    global instantaneousActivePowerExportL3
    
    global totalEnergyConsumption
    global solarPanelGeneration
    
    global gasReading

    global powerDeliveredHistory
    global powerExportedHistory
    
    previousActiveEnergyImport = activeEnergyImport
    previousActiveEnergyExport = activeEnergyExport
    
    previousActiveEnergyImportLowTarrif = activeEnergyImportLowTarrif
    previousActiveEnergyExportLowTarrif = activeEnergyExportLowTarrif

    previousActiveEnergyImportNormalorHighTarrif = activeEnergyImportNormalorHighTarrif
    previousActiveEnergyExportNormalorHighTarrif = activeEnergyExportNormalorHighTarrif

    previousImportWattHour = importWattHour
    previousExportWattHour = exportWattHour

    for m in pattern.finditer(mqttTelegram):
        for line in m.string.splitlines():
            activeEnergyImport = float(parse('1-0:1.8.0*255({}*kWh)',line)[0]) if parse('1-0:1.8.0*255({}*kWh)',line) is not None else activeEnergyImport
            activeEnergyImportLowTarrif = float(parse('1-0:1.8.1*255({}*kWh)',line)[0]) if parse('1-0:1.8.1*255({}*kWh)',line) is not None else activeEnergyImportLowTarrif
            activeEnergyImportNormalorHighTarrif = float(parse('1-0:1.8.2*255({}*kWh)',line)[0]) if parse('1-0:1.8.2*255({}*kWh)',line) is not None else activeEnergyImportNormalorHighTarrif

            activeEnergyExport = float(parse('1-0:2.8.0*255({}*kWh)',line)[0]) if parse('1-0:2.8.0*255({}*kWh)',line) is not None else activeEnergyExport
            activeEnergyExportLowTarrif = float(parse('1-0:2.8.1*255({}*kWh)',line)[0]) if parse('1-0:2.8.1*255({}*kWh)',line) is not None else activeEnergyExportLowTarrif
            activeEnergyExportNormalorHighTarrif = float(parse('1-0:2.8.2*255({}*kWh)',line)[0]) if parse('1-0:2.8.2*255({}*kWh)',line) is not None else activeEnergyExportNormalorHighTarrif
        
            voltagePhaseOne = float(parse('1-0:32.7.0*255({}*V)', line)[0]) if parse('1-0:32.7.0*255({}*V)', line) is not None else voltagePhaseOne
            voltagePhaseTwo = float(parse('1-0:52.7.0*255({}*V)', line)[0]) if parse('1-0:52.7.0*255({}*V)', line) is not None else voltagePhaseTwo
            voltagePhaseThree = float(parse('1-0:72.7.0*255({}*V)', line)[0]) if parse('1-0:72.7.0*255({}*V)', line) is not None else voltagePhaseThree

            currentPhaseOne = float(parse('1-0:31.7.0*255({}*A)', line)[0]) if parse('1-0:31.7.0*255({}*A)', line) is not None else currentPhaseOne
            currentPhaseTwo = float(parse('1-0:51.7.0*255({}*A)', line)[0]) if parse('1-0:51.7.0*255({}*A)', line) is not None else currentPhaseTwo
            currentPhaseThree = float(parse('1-0:71.7.0*255({}*A)', line)[0]) if parse('1-0:71.7.0*255({}*A)', line) is not None else currentPhaseThree

    totalEnergyConsumption = (currentPhaseOne * voltagePhaseOne) + (currentPhaseTwo * voltagePhaseTwo) + (currentPhaseThree * voltagePhaseThree)
    
    # Check which tarrif specific reading has changed, and thus determining the active tarrif setting
    if activeEnergyImportLowTarrif - previousActiveEnergyImportLowTarrif > 0:
        activeTarrif = 2
        powerExportedHistory.append((activeEnergyExportLowTarrif,time.time()))
        powerImportedHistory.append((activeEnergyImportLowTarrif,time.time()))
    elif activeEnergyImportNormalorHighTarrif - previousActiveEnergyImportNormalorHighTarrif > 0:
        activeTarrif = 1
        powerExportedHistory.append((activeEnergyExportNormalorHighTarrif,time.time()))
        powerImportedHistory.append((activeEnergyImportNormalorHighTarrif,time.time()))
    # time.time() is already in seconds, with a floating point as milliseconds precision
    timeDifference = powerImportedHistory[len(powerImportedHistory)-1][1] - powerImportedHistory[0][1]
#    if timeDifference > 14: # Wait about two readouts
#        previousMessage = 0
#        for item in powerDeliveredHistory:
#            powerDeliveredHistory.remove(item)
#        for item in powerExportedHistory:
#            powerExportedHistory.remove(item)


    if timeDifference != 0:
        # Take 15 readings
        #print('Reading')
        if len(powerImportedHistory) >= 4:
            powerImportedHistory.pop(0)
        if len(powerExportedHistory) >= 4:
            powerExportedHistory.pop(0) 

        importWattHour = (powerImportedHistory[len(powerImportedHistory)-1][0] - powerImportedHistory[0][0])*1000
        exportWattHour = (powerExportedHistory[len(powerExportedHistory)-1][0] - powerExportedHistory[0][0])*1000
            
        #importWattHour = previousImportWattHour if importWattHour == 0.0 else importWattHour
        #exportWattHour = previousExportWattHour if exportWattHour == 0.0 else exportWattHour

        #actualPowerDeliveredToClient = importWattHour / (timeDifference / 3600.0) / 1000
        #actualPowerReceivedFromClient = exportWattHour / (timeDifference / 3600.0) / 1000
        
        actualPowerDeliveredToClient = ((totalEnergyConsumption - solarPanelGeneration) / 1000) if totalEnergyConsumption > solarPanelGeneration else 0
        actualPowerReceivedFromClient = (abs(totalEnergyConsumption - solarPanelGeneration) / 1000) if solarPanelGeneration > totalEnergyConsumption else 0

        if debug:
            print("actualPowerDeliveredToClient is not totalEnergyConsumption: " + str(actualPowerDeliveredToClient is not totalEnergyConsumption))
            print("actualPowerReceivedFromClient is 0.0: " + str(actualPowerReceivedFromClient == 0.0))
            print("actualPowerReceivedFromClient: " + str(actualPowerReceivedFromClient))
        
        if actualPowerDeliveredToClient == 0.0 and totalEnergyConsumption != 0.0:
            if debug:
                print("Less energy is used than the solar panels generate")
            # The energy comming from the solar panels is used 
            # Subtract from the average remaining kW
            instantaneousActivePowerExportL1 = abs((actualPowerReceivedFromClient/3)-(currentPhaseOne * voltagePhaseOne * 0.001) )
            instantaneousActivePowerExportL2 = abs((actualPowerReceivedFromClient/3)-(currentPhaseTwo * voltagePhaseTwo * 0.001) )
            instantaneousActivePowerExportL3 = abs((actualPowerReceivedFromClient/3)-(currentPhaseThree * voltagePhaseThree * 0.001) )
            
        elif (actualPowerDeliveredToClient*1000) is not totalEnergyConsumption and actualPowerReceivedFromClient == 0.0:
            if debug:
                print("More energy is used than the solar panels generate")
            # The energy comming from the solar panels is not sufficient to power everything
            instantaneousActivePowerImportL1 = abs((currentPhaseOne * voltagePhaseOne * 0.001) - (actualPowerDeliveredToClient/3))
            instantaneousActivePowerImportL2 = abs((currentPhaseTwo * voltagePhaseTwo * 0.001) - (actualPowerDeliveredToClient/3))
            instantaneousActivePowerImportL3 = abs((currentPhaseThree * voltagePhaseThree * 0.001) - (actualPowerDeliveredToClient/3))
        
        #solarPanelGeneration = totalEnergyConsumption - (actualPowerDeliveredToClient*1000)
            
        if debug:                   
            print("TD: " + str(timeDifference))
            print("importWattHour: " + str(importWattHour))
            print("exportWattHour: " + str(exportWattHour))
            print("previousImportLowTarrif: " + str(previousActiveEnergyImportLowTarrif))
            print("previousExportLowTarrif: " + str(previousActiveEnergyExportLowTarrif))
            print("previousImportNormalorHighTarrif: " + str(previousActiveEnergyImportNormalorHighTarrif))
            print("previousExportNormalorHighTarrif: " + str(previousActiveEnergyExportNormalorHighTarrif))
        
    previousMessage = time.time()
        
    if debug:  
        print("total energy consumption: " + str(totalEnergyConsumption))
        print("the solar panels generate: " + str(solarPanelGeneration))
        print("1.7.0: " + str(actualPowerDeliveredToClient))
        print("1.8.0: " + str(activeEnergyImport))
        print("1.8.1: " + str(activeEnergyImportLowTarrif))
        print("1.8.2: " + str(activeEnergyImportNormalorHighTarrif))
        print("2.7.0: " + str(actualPowerReceivedFromClient))
        print("2.8.0: " + str(activeEnergyExport))
        print("2.8.1: " + str(activeEnergyExportLowTarrif))
        print("2.8.2: " + str(activeEnergyExportNormalorHighTarrif))

receivedTelegram = False
msg = ""

def on_message(client, userdata, msg):
    # Fill in all the values
    previousActiveEnergyImport = activeEnergyImport
    previousActiveEnergyExport = activeEnergyExport
    if debug:  
        print("Received MQTT: " + msg.payload + "\r\n")
    parseTelegram(msg.payload + "!\r\n")
    injectedTelegram = fillTelegram()
    
    global receivedTelegram
    receivedTelegram = True

def calculateCRC(injectedTelegram):
    calculated_checksum = 0
    for m in pattern.finditer(injectedTelegram):
      # Remove the exclamation mark from the checksum,
      # and make an integer out of it.
      # given_checksum = "0x" + int(telegram[m.end() + 1:].decode('ascii'), 16)
      # The exclamation mark is also part of the text to be CRC16'd
      calculated_checksum = crc16(injectedTelegram[:m.end() + 1])
    return calculated_checksum

def fillTelegram():
    timeString = "{:0>2d}".format(datetime.datetime.now().year-2000) + "{:0>2d}".format(datetime.datetime.now().month) + "{:0>2d}".format(datetime.datetime.now().day) + "{:0>2d}".format(datetime.datetime.now().hour) + "{:0>2d}".format(datetime.datetime.now().minute) + "{:0>2d}".format(datetime.datetime.now().second) + "S" if time.localtime().tm_isdst is 1 else "W"

    injectedTelegram =  "/ISk5\MT174-001\r\n\r\n\
1-0:0.0.0(64194884)\r\n\
1-3:0.2.8(50)\r\n\
0-0:1.0.0({})\r\n\
0-0:96.1.1(4530303131303036343139343838343135)\r\n\
1-0:1.8.1({:0>10.3f}*kWh)\r\n\
1-0:1.8.2({:0>10.3f}*kWh)\r\n\
1-0:2.8.1({:0>10.3f}*kWh)\r\n\
1-0:2.8.2({:0>10.3f}*kWh)\r\n\
0-0:96.14.0({:0>4})\r\n\
1-0:1.7.0({:0>6.3f}*kW)\r\n\
1-0:2.7.0({:0>6.3f}*kW)\r\n\
0-0:96.7.21(00000)\r\n\
0-0:96.7.9(00000)\r\n\
1-0:99.97.0(0)(0-0:96.7.19)\r\n\
1-0:32.32.0(00000)\r\n\
1-0:52.32.0(00000)\r\n\
1-0:72.32.0(00000)\r\n\
1-0:32.36.0(00000)\r\n\
1-0:52.36.0(00000)\r\n\
1-0:72.36.0(00000)\r\n\
0-0:96.13.0()\r\n\
1-0:32.7.0({:0>5.1f}*V)\r\n\
1-0:52.7.0({:0>5.1f}*V)\r\n\
1-0:72.7.0({:0>5.1f}*V)\r\n\
1-0:31.7.0({:0>3.0f}*A)\r\n\
1-0:51.7.0({:0>3.0f}*A)\r\n\
1-0:71.7.0({:0>3.0f}*A)\r\n\
1-0:21.7.0({:0>6.3f}*kW)\r\n\
1-0:22.7.0({:0>6.3f}*kW)\r\n\
1-0:41.7.0({:0>6.3f}*kW)\r\n\
1-0:42.7.0({:0>6.3f}*kW)\r\n\
1-0:61.7.0({:0>6.3f}*kW)\r\n\
1-0:62.7.0({:0>6.3f}*kW)\r\n\
0-1:24.1.0(000)\r\n\
!"
# 0-1:24.2.1({})({:0>9.3f}*m3)\r\n\
    global activeEnergyImport
    global activeEnergyImportLowTarrif
    global activeEnergyImportHighTarrif
    global activeEnergyImportNormalorHighTarrif
    global activeEnergyExport
    global activeEnergyExportLowTarrif
    global activeEnergyExportHighTarrif
    global activeEnergyExportNormalorHighTarrif
    global activeTarrif
    
    global instantaneousActivePowerImportL1
    global instantaneousActivePowerImportL2
    global instantaneousActivePowerImportL3
    
    global instantaneousActivePowerExportL1
    global instantaneousActivePowerExportL2
    global instantaneousActivePowerExportL3
    
    injectedTelegram = injectedTelegram.format(timeString,activeEnergyImportLowTarrif,activeEnergyImportNormalorHighTarrif,activeEnergyExportLowTarrif,activeEnergyExportNormalorHighTarrif, activeTarrif, actualPowerDeliveredToClient,actualPowerReceivedFromClient,voltagePhaseOne,voltagePhaseTwo,voltagePhaseThree,round(currentPhaseOne),round(currentPhaseTwo),round(currentPhaseThree),
    instantaneousActivePowerImportL1,instantaneousActivePowerImportL2,instantaneousActivePowerImportL3,instantaneousActivePowerExportL1,instantaneousActivePowerExportL2,instantaneousActivePowerExportL3) #, timeString, gasReading)
    # According to the source code, sending data without a CRC should do something
    checksum = calculateCRC(injectedTelegram) 
    injectedTelegram += format(checksum,"04X") + "\r\n"
    if debug:  
        #print "Filled telegram: " + injectedTelegram
        pass
    return injectedTelegram

global lastTelegram
    
client = mqtt.Client()
client.tls_set(ca_certs='/volume1/Downloads/ca.crt', certfile='/volume1/Downloads/SYNOLOGY.crt', keyfile='/volume1/Downloads/SYNOLOGY.key', cert_reqs=ssl.CERT_REQUIRED,tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)# Register publish callback function
client.connect('dashboard', 8884,45)
client.on_message = on_message
client.subscribe("P1/Raw")
receivedTelegram = False
lastTelegram = datetime.datetime.now()
shouldWait = True
msg = ""
solarEdgeCounter = 0
while True:
    client.loop(.001)
    
    if shouldWait or not receivedTelegram:
        # Wait for the Toon to stop requesting data
        shouldWait = not ser.rts
        msg = fillTelegram()
    else:
        if ser.rts:
            msg = fillTelegram()
            if abs(datetime.datetime.now().second - lastTelegram.second) >= 1:
               if debug:  
                   print "Sending " + msg
                   pass
               try:
                   ser.write(msg)
                   ser.flush()
                   client.publish("P1/DSMRv5.0", msg)
               except:
                   print("Exception!")
                   pass

               lastTelegram = datetime.datetime.now()
               shouldWait = True
               if solarEdgeCounter == 300:
                   r = requests.get(solaredge)
                   if r.status_code == 200:
                       solarPanelGeneration = r.json()["overview"]["currentPower"]["power"]
                   else:
                       solarPanelGeneration = 0
                   solarEdgeCounter = 0
               else:
                   solarEdgeCounter += 1
            else:
                pass    

exit(0)
                                                            
